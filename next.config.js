/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,

  images: {
    domains: ['dci-storage.privydev.id'],
  },
  async rewrites() {
    return [
      {
        source: '/api/v1/:path*',
        destination: 'http://pretest-qa.dcidev.id/api/v1/:path*'
      },
    ]
  }
}

module.exports = nextConfig
