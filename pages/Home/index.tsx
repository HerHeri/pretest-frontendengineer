import React from 'react';
import axios from "axios"
import Cookies from "js-cookie"
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import * as cookie from 'cookie'
import ReactDOM from "react-dom";
import Image from 'next/image'
import Modal from 'react-modal'
import DataProfile from '../../components/DataProfile';
import { PopoverBody, PopoverHeader, UncontrolledPopover } from "reactstrap";
import DataPendidikan from '../../components/DataPendidikan';
import DataPekerjaan from '../../components/DataPekerjaan';

export default (props:any) => {
    
    // let subtitle;
    
    const [inputProfileImage, stateProfileImage] = useState('');
    const {dataMeProfile} = props
    const Router = useRouter()
    const data = Cookies.get('user_token') ?? '{}'
    const user_token = JSON.parse(data)    
    const [modalIsOpenProfile, setIsOpenProfile] = React.useState(false);
    const [modalIsOpenPendidikan, setIsOpenPendidikan] = React.useState(false);
    const [modalIsOpenPekerjaan, setIsOpenPekerjaan] = React.useState(false);

    coverStyle.backgroundImage = `url(${dataMeProfile.cover_picture.url ?? "/cover.jpg"})`

    // Update Profile Picture
    function updateProfilePicture() {
        const data = new FormData()
        data.append('token', user_token.access_token)
        data.append('image', inputProfileImage)
        axios.post('http://localhost:3000/api/Profile/changePictureProfile', data
        ,{
            headers: {
                'Content-Type': `multipart/form-data;`,
            }
        }).then((r) => {
            console.log('info', r);
            Router.push('/Home')
        })
        .catch(e => console.log(e))
    }

    function closeModalProfile() {
        setIsOpenProfile(false);
    }

    function openModalProfile() {
        setIsOpenProfile(true);
    }
    
    function openModalPendidikan() {
        setIsOpenPendidikan(true);
    }

    function closeModalPendidikan() {
        setIsOpenPendidikan(false);
    }

    function openModalPekerjaan() {
        setIsOpenPekerjaan(true);
    }

    function closeModalPekerjaan() {
        setIsOpenPekerjaan(false);
    }
    
    function backToLogin() {
        useEffect(()=>{
            Router.push('/Auth/login')
        }, [])
    }
    
    if(user_token?.access_token){
        axios.get(`http://pretest-qa.dcidev.id/api/v1/oauth/credentials?access_token=${user_token.access_token}`)
        .then(function (response) {
            // Router.push('/Auth/otp')
        })
        .catch(function (error) {
            logout()
        });
    }else{
        backToLogin()
    }

    function logout() {
        Cookies.remove('user_token')
        Cookies.remove('user_id')
        Cookies.remove('data_user')
        Router.push('/Auth/login')  
    }

    return (
        <>
        <nav className="navbar navbar-light" style={{backgroundColor: '#e3f2fd'}}>
            <div className="container">
                <a className="navbar-brand">Pretest FrontEnd App</a>
                <div className="d-flex">
                    <div className="ps-3 mt-2">
                        <icon className="fa-solid fa-bell fa-xl notif-icon"></icon>
                    </div>
                    <div className="ps-3">
                        <div className="rounded-circle" id="popover73472958">
                            <img src={"/profile.jpg" } 
                            style={{ height:"50px", width:"50px"}} className="img-fluid rounded-circle rounded-shadow" />
                        </div>
                        <UncontrolledPopover placement="bottom" target="popover73472958" className="popover-style">
                            <PopoverHeader >
                                {dataMeProfile.name}
                            </PopoverHeader>
                            <PopoverBody>
                                <button className="btn btn-outline-danger btn-sm mt-2" type="submit" onClick={logout}>Logout</button>
                            </PopoverBody>
                        </UncontrolledPopover>
                    </div>
                </div>
            </div>
        </nav>
        <div className="container">
            <div className="row justify-content-center mt-4">
                <div className="col-md-12">
                    <div className="card justify-content-center">
                        <div className="w-100" style={coverStyle} data-image={"/cover.jpg"}></div>
                        {/* <img src={ dataMeProfile.cover_picture.url ?? "/cover.jpg" } className="img-fluid w-100" alt="Responsive image" /> */}
                    </div>
                </div> 
            </div>
            <div className="row">
                <div className="col-md-4">
                    <div className="d-flex justify-content-center">
                        <div className="rounded-circle" style={photoStyle}>
                            <img src={ "/profile.jpg" } 
                            style={{ height:"200px", width:"200px"}} className="img-fluid rounded-circle rounded-shadow" />
                        </div>
                    </div>
                    <div className="row d-flex justify-content-center">
                        <div className="col-12">
                            <div className="input-group ps-4 mt-3" >
                                <div className="custom-file">
                                    <input type="file" className="custom-file-input" id="inputGroupFile04" value={inputProfileImage} 
                                    onChange={e => stateProfileImage(e.target.value)} />
                                </div>
                                <div className="input-group-append">
                                    <button className="btn btn-outline-info btn-sm" style={buttonDown} onClick={updateProfilePicture} type="button">Change</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* Data Profile */}
                    <div className="row mt-5">
                        <div className="col-md-12">
                            <div className="card mb-4 p-3">
                                    <h5>Data Profile</h5>
                                    <hr />
                                    <div className="form-group row mb-2">
                                        <label className="col-sm-4 col-form-label">Nama</label>
                                        <label className="col-sm-1 col-form-label">:</label>
                                        <div className="col-sm-7 col-form-label">
                                            <span><b> {dataMeProfile.name} </b></span>
                                        </div>
                                    </div>
                                    <div className="form-group row mb-2">
                                        <label className="col-sm-4 col-form-label">Umur</label>
                                        <label className="col-sm-1 col-form-label">:</label>
                                        <div className="col-sm-7 col-form-label">
                                            <span><b> {dataMeProfile.age} </b></span>
                                        </div>
                                    </div>
                                    <div className="form-group row mb-2">
                                        <label className="col-sm-4 col-form-label">Tanggal Lahir</label>
                                        <label className="col-sm-1 col-form-label">:</label>
                                        <div className="col-sm-7 col-form-label">
                                            <span><b> {dataMeProfile.birthday} </b></span>
                                        </div>
                                    </div>
                                    <div className="form-group row mb-2">
                                        <label className="col-sm-4 col-form-label">Jenis Kelamin</label>
                                        <label className="col-sm-1 col-form-label">:</label>
                                        <div className="col-sm-7 col-form-label">
                                            <span><b> {dataMeProfile.gender} </b></span>
                                        </div>
                                    </div>
                                    <div className="form-group row mb-2">
                                        <label className="col-sm-4 col-form-label">Zodiac</label>
                                        <label className="col-sm-1 col-form-label">:</label>
                                        <div className="col-sm-7 col-form-label">
                                            <span><b> {dataMeProfile.zodiac} </b></span>
                                        </div>
                                    </div>
                                    <div className="form-group row mb-2">
                                        <label className="col-sm-4 col-form-label">Alamat</label>
                                        <div className="col-sm-12 col-form-label">
                                            <span><b> {dataMeProfile.hometown} </b></span>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-sm-4 col-form-label">Tentang Saya</label>
                                        <div className="col-sm-12 col-form-label">
                                            <span><b> {dataMeProfile.bio} </b></span>
                                        </div>
                                    </div>
                                    <button className="btn btn-success btn-md btn-rounded float-end mt-5" 
                                    onClick={openModalProfile} >Update Profile</button>
                            </div>
                        </div>
                    </div>
                </div>

                {/* Riwayat Kerja dan Riwayat Pendidikan */}
                <div className="col-md-8 mt-5 mb-4">
                    <div className="row">
                        <div className="col-12">
                            <div className="card mb-5 p-3">
                                <div className="row">
                                    <div className="col-6">
                                        <h5 >Riwayat Pendidikan</h5>
                                    </div>
                                    <div className="col-6">
                                        <button className="btn btn-success btn-md btn-rounded float-end" 
                                        data-toggle="modal" onClick={openModalPendidikan} >Update Data Riwayat</button>
                                    </div>
                                </div>
                                <table className="table">
                                    <thead>
                                        <tr>
                                        <th scope="col">Nama Sekolah</th>
                                        <th scope="col">Tahun Lulus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{dataMeProfile.education.school_name}</td>
                                            <td>{dataMeProfile.education.graduation_time}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div className="col-12">
                            <div className="card mb-5 p-3">
                                <div className="row">
                                    <div className="col-6">
                                        <h5 >Riwayat Pekerjaan</h5>
                                    </div>
                                    <div className="col-6">
                                        <button className="btn btn-success btn-md btn-rounded float-end" 
                                        data-toggle="modal" onClick={openModalPekerjaan} >Update Data Pekerjaan</button>
                                    </div>
                                </div>
                                <table className="table">
                                    <thead>
                                        <tr>
                                        <th scope="col">Nama Perusahaan</th>
                                        <th scope="col">Tahun Masuk</th>
                                        <th scope="col">Tahun Keluar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{dataMeProfile.career.company_name}</td>
                                            <td>{dataMeProfile.career.starting_from}</td>
                                            <td>{dataMeProfile.career.ending_in}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="card mb-5 p-3">
                                <h5>Portofolio Project</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {/* Modal Riwayat Pendidikan */}
        <DataProfile 
            dataMeProfile={dataMeProfile} 
            showModal={modalIsOpenProfile} 
            token={user_token.access_token} 
            onClose={closeModalProfile} 
        />

        {/* Modal Riwayat Pendidikan */}
        <DataPendidikan 
            dataMeProfile={dataMeProfile} 
            showModal={modalIsOpenPendidikan} 
            token={user_token.access_token} 
            onClose={closeModalPendidikan} 
        />

        {/* Modal Riwayat Pekerjaan */}
        <DataPekerjaan 
            dataMeProfile={dataMeProfile} 
            showModal={modalIsOpenPekerjaan} 
            token={user_token.access_token} 
            onClose={closeModalPekerjaan} 
        />
        </>
    )
}

const coverStyle = {
    backgroundImage: "url()",
    backgroundPosition: "center",
    backgroundSize: "cover",
    backgroundColor: "#fff",
    height: "300px"
}

const photoStyle = {
    height: "200px",
    width: "200px",
    marginTop: "-100px",
    zIndex: "0",
    backgroundColor: "grey"
}

const buttonDown = {
    zIndex: "0"
}

export async function getServerSideProps(context:any) {
    const data = cookie.parse(context.req.headers.cookie ?? '');
    if (!data?.user_token) {
        return {
            redirect: {
                permanent: false,
                destination: "/Auth/login"
            }
        }
    }
    const user_token = JSON.parse(data.user_token)
    const variabel = await axios.get('http://pretest-qa.dcidev.id/api/v1/profile/me', { headers: {
            "Authorization": `Bearer ${user_token?.access_token ?? ''}`
            }   
        })
        .catch(function (error) {
            console.log(error);
            return error
        });
    return {
        props: {
            dataMeProfile: variabel.data.data.user
        }
    }
    
}

