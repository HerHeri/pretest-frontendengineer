import axios from "axios";
import { useRouter } from "next/router";
import { Component, useState } from "react"
import Cookies from 'js-cookie'

export default () => {
    const [inputPass, statePass] = useState('');
    const [inputPhone, statePhone] = useState('');
    const Router = useRouter()

    function submitLogin() {
        axios.post('http://pretest-qa.dcidev.id/api/v1/oauth/sign_in', {
            phone: inputPhone,
            password: inputPass,
            latlong: "-",
            device_token: Math.floor(Math.random() * 100 + 1),
            device_type: 2 // sementara web
        })
        .then(function (response) {
            if(response.statusText == "Created"){
                const user = response.data.data.user
                Cookies.set('user_token', JSON.stringify(user))
                Router.push('/Home')
            }
        })
        .catch(function (error) {
            const err = error.response.data?.error.errors;
            if(err[0] == "Invalid Password"){
                statePass("")
                console.log(err[0]);
            }else if(err[0].message == "You need verification phone number"){
                Cookies.set('user_id', err[0].user_id)
                Cookies.set('data_user', JSON.stringify({phone: inputPhone}))
                Router.push('/Auth/otp')
            }else{
                console.log(err);
            }
        });
    }

    return (
        <>
        <div className="box-center-web">
            <div className="container">
                <div className="row justify-content-center mt-4">
                    <div className="col-md-4">
                        <div className="card text-center" style={{ backgroundColor:"#F0F0F0" }}>
                            <div className="row justify-content-center mb-4">
                                <h5 className="mt-4 mb-4">Login</h5>
                            </div>
                            <div className="mb-3 row justify-content-center">
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" id="inputPhone" onChange={e => statePhone(e.target.value)} placeholder="Phone" /><br />
                                    <input type="password" className="form-control" id="inputPassword" onChange={e => statePass(e.target.value)} placeholder="Password" value={inputPass} />
                                </div>
                                <div className="col-10">
                                    <hr />
                                    <button type="submit" className="btn btn-primary" onClick={(e)=>submitLogin()}>Login</button>
                                    <br /><br />
                                    <span>Belum Punya Akun? &nbsp;</span><a href="/Auth/registration"><u>Register</u></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}