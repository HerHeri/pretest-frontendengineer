import axios from 'axios';
import { useRouter } from 'next/router';
import React, { Component, useState } from 'react';
import OtpInput from 'react-otp-input';
import Cookies from 'js-cookie'
export default () => {
    const [otpInput, stateOtp] = useState('');
    const router = useRouter()
    
    function handleChange (otp: any) { 
        stateOtp(otp);
    }
    
    function sendOtp(){
        var user_id = Cookies.get('user_id')
        console.log(user_id);
        
        
        if(otpInput.length != 4){
            console.log("Lengkapi data");
        }else{
            axios.post('http://pretest-qa.dcidev.id/api/v1/register/otp/match', {
                user_id,
                otp_code: otpInput,
            })
            .then(function (response) {
                console.log(response);
                if(response.statusText == "Created"){
                    const user = response.data.data.user
                    router.push('/Auth/login')
                }
            })
            .catch(function (error) {
                console.error(error);
            });
        }
    }

    function getOtp() {
        const data_user = Cookies.get('data_user') ?? '{}'
        const data = JSON.parse(data_user)
        axios.post('http://pretest-qa.dcidev.id/api/v1/register/otp/request', {
            phone: data.phone,
            })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.error(error);
            });
    }

    return (
        <>
        <div className="box-center-web">
            <div className="container">
                <div className="row justify-content-center mt-4">
                    <div className="col-md-4">
                        <div className="card text-center" style={{ backgroundColor:"#F0F0F0" }}>
                            <div className="row justify-content-center mb-4">
                                <h5 className="mt-4 mb-4">Percobaan Otp</h5>
                            </div>
                            <div className="mb-3 row justify-content-center">
                                <div className="col-sm-12">
                                    <div className="d-flex justify-content-center mb-4">
                                        <OtpInput
                                            value={otpInput}
                                            onChange={handleChange}
                                            numInputs={4}
                                            separator={<span>-</span>}
                                            inputStyle={{ 
                                                textAlign: "center",
                                                margin: "0 10px",
                                                color: "black",
                                                fontSize: "24px",
                                                width: "40px",
                                                height: "48px",
                                            }}
                                        />
                                    </div>
                                    <a type="button" onClick={(e)=> sendOtp()} className="btn btn-primary mt-2">Verifikasi</a>
                                </div>
                                <div className="col-10">
                                    <hr />
                                    <span><u>
                                        <button type="button" onClick={(e)=> getOtp()} className="btn btn-link mt-2">Kirim Ulang Kode OTP</button>
                                        </u></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}