import axios from "axios";
import { useRouter } from "next/router";
import { useState } from "react"
import Cookies from 'js-cookie'
export default () =>  {
    
    const [inputPass, statePass] = useState('');
    const [inputPhone, statePhone] = useState('');
    const [inputCountry, stateCountry] = useState('');
    const Router = useRouter()
            
        function submitRegister() {

            axios.post('http://pretest-qa.dcidev.id/api/v1/register', {
                phone: inputPhone,
                country: inputCountry,
                password: inputPass,
                latlong: "-",
                device_token: Math.floor(Math.random() * 100 + 1),
                device_type: 2 // sementara web
            })
            .then(function (response) {
                if(response.statusText == "Created"){
                    const user = response.data.data.user
                    Cookies.set('data_user', JSON.stringify(user))
                    Cookies.set('user_id', user.id)
                    Router.push('/Auth/otp')
                }
            })
            .catch(function (error) {
                if( error.response?.data.error.errors[0] == "Phone has already been taken"){
                    const phone = JSON.stringify({phone: inputPhone})
                    Cookies.set('data_user', phone)
                    Router.push('/Auth/login')
                    console.log("Nomor Sudah Terdaftar");
                }
            });
        }

        return (
            <>
            <div className="box-center-web">
                <div className="container">
                    <div className="row justify-content-center mt-4">
                        <div className="col-md-4">
                            <div className="card text-center" style={{ backgroundColor:"#F0F0F0" }}>
                                <div className="row justify-content-center mb-4">
                                    <h5 className="mt-4 mb-4">Registration</h5>
                                </div>
                                <div className="mb-3 row justify-content-center">
                                    <div className="col-sm-10">
                                        <input type="text" className="form-control" placeholder="Phone" value={inputPhone} onChange={e => statePhone(e.target.value)} /><br />
                                        <input type="password" className="form-control" placeholder="Password" value={inputPass} onChange={e => statePass(e.target.value)} /><br />
                                        <input type="text" className="form-control" placeholder="Country" value={inputCountry} onChange={e => stateCountry(e.target.value)} />
                                    </div>
                                    <div className="col-10">
                                        <hr />
                                        <button type="submit" className="btn btn-primary" onClick={(e)=>submitRegister()}>Register</button>
                                        <br /><br />
                                        <span>Sudah Punya Akun? &nbsp;</span><a href="/Auth/login"><u>Login</u></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </>
        )
}