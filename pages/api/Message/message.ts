// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import axios from 'axios'
import type { NextApiRequest, NextApiResponse } from 'next'

type Data = {
  res: any
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {

    const {
        token,
        user_id
    } = req.body

    let status = 200;

    const resSweger = await axios.get(`http://pretest-qa.dcidev.id/api/v1/message/${user_id}`, { headers: {
            "Authorization": `Bearer ${token}`
            }
        }).then(function (res) {
            return res.data.data
        })
        .catch(function (error) {
            console.log(error);
            return error
        });
    return {
        props: {
            message: resSweger
        }
    }
}
