// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import axios from 'axios'
import type { NextApiRequest, NextApiResponse } from 'next'
import FormData from 'form-data'
import {Formidable} from 'formidable'

//set bodyparser
export const config = {
  api: {
    bodyParser: false
  }
}

type Data = {
  res: any,
  message: string
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {  
    const dataForm:any = await new Promise((resolve, reject) => {
      let form = new Formidable()

      form.parse(req, (err, fields, files) => {
        if (err) return reject({ err })
        resolve({ fields, files })
      }) 
    })
    
    const resSweger = await axios.post('http://pretest-qa.dcidev.id/api/v1/uploads/profile', req.body,
        {
            headers: {
                "Authorization": `Bearer ${dataForm.fields.token}`,
                'accept': 'application/json',
                'Accept-Language': 'en-US,en;q=0.8',
                'Content-Type': `multipart/form-data;`,
            }
        })
        .catch(function (error) {
            console.log(error);
            return error
        });
        
  res.status(resSweger.response.status).json({
      message: resSweger.response.statusText,
      res: resSweger
  })
}
