import { NextApiRequest, NextApiResponse } from 'next'
import { Formidable } from 'formidable'

//set bodyparser
export const config = {
  api: {
    bodyParser: false
  }
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
    // const {
    //     // token,
    //     // image
    //     data
    // } = req.body

  const dataForm = await new Promise((resolve, reject) => {
    let form = new Formidable()

    form.parse(req, (err:any, fields:any, files:any) => {
      if (err) reject({ err })
      resolve({ err, fields, files })
    }) 
  })

  console.log(dataForm.fields.token);
  

  //return the data back or just do whatever you want with it
  res.status(200).json({
    status: 'ok',
    dataForm
  })
}