// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import axios from 'axios'
import type { NextApiRequest, NextApiResponse } from 'next'

type Data = {
  res: any
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {

    const {
        token,
        hometown,
        birthday,
        name,
        gender,
        bio
    } = req.body

    const resSweger = await axios.post('http://pretest-qa.dcidev.id/api/v1/profile', 
        {
            hometown,
            birthday,
            name,
            gender,
            bio
        },
        {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
        .catch(function (error) {
            console.log(error);
            return error
        });

  res.status(200).json({
      res: resSweger
  })
}
