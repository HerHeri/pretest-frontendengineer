// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import axios from 'axios'
import type { NextApiRequest, NextApiResponse } from 'next'

type Data = {
  res: any
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {

    const {
        token,
        company_name,
        starting_from,
        ending_in,
        position,
    } = req.body

    // console.log(req.body);
    
    let status = 200;

    const resSweger = await axios.post('http://pretest-qa.dcidev.id/api/v1/profile/career', 
        {
            company_name,
            starting_from,
            ending_in,
            position,
        },
        {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
        .catch((error) => {
            console.log("error sweger", error);
            status = 400
            return error
        });
        

    res.status(status).json({
        res: "Ok!"
    })
}
