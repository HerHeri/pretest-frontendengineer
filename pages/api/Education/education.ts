// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import axios from 'axios'
import type { NextApiRequest, NextApiResponse } from 'next'

type Data = {
  res: any
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {

    let status = 200;
    const {
        token,
        school_name,
        graduation_time,
    } = req.body
    
    const resSweger = await axios.post('http://pretest-qa.dcidev.id/api/v1/profile/education', 
        {
            school_name,
            graduation_time,
        },
        {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
        .catch(function (error) {
            console.log(error);
            status = 400;
            return error
        });

  res.status(status).json({
      res: "Ok!"
  })
}
