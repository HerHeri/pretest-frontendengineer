import Modal from 'react-modal'
import { useEffect, useState } from "react";
import Router from 'next/router';
import axios from 'axios';
import React from 'react';

export default function DataProfile(props:any) {
    const [inputName, stateName] = useState('');
    const [inputBirthday, stateBirthday] = useState('');
    const [inputGender, stateGender] = useState('');
    const [inputHometown, stateHometown] = useState('');
    const [inputBio, stateBio] = useState('');
    const {
        dataMeProfile, 
        showModal,
        onClose,
        token
    } = props
    
    console.log(props);
    
    // Update Profile
    function updateProfile() {
        axios.post('http://localhost:3000/api/Profile/profile', {
            token,
            name: inputName,
            gender: inputGender,
            birthday: inputBirthday,
            hometown: inputHometown,
            bio: inputBio
        }).then((r) => {
            onClose()
            Router.push('/Home')
        })
        .catch(e => console.log(e))
    }

    return (
        
        <Modal
            isOpen={showModal}
            onRequestClose={onClose}
            ariaHideApp={false}
        >
            <div className="modal-overlay" >
                <div className="modal-style position-relative" style={{ zIndex: "20" }}>
                    <div className="modal-header d-flex">
                        <h4>Edit Profile</h4>
                        <div className="d-flex float-end">
                            <button className="btn btn-danger m-1" onClick={onClose}>Close</button>
                        </div>
                    </div>
                    <div className="modal-body">
                        <div className="form-group row mb-2">
                            <label className="col-sm-4 col-form-label">Nama</label>
                            <label className="col-sm-1 col-form-label">:</label>
                            <div className="col-sm-7">
                                <input type="text" className="form-control" value={inputName} placeholder={dataMeProfile.name}
                                onChange={e => stateName(e.target.value)} />
                            </div>
                        </div>
                        <div className="form-group row mb-2">
                            <label className="col-sm-4 col-form-label">Tanggal Lahir</label>
                            <label className="col-sm-1 col-form-label">:</label>
                            <div className="col-sm-7">
                                <input type="date" className="form-control" value={inputBirthday} placeholder={dataMeProfile.birthday}
                                onChange={e => stateBirthday(e.target.value)} />
                            </div>
                        </div>
                        <div className="form-group row mb-2">
                            <label className="col-sm-4 col-form-label">Jenis Kelamin</label>
                            <label className="col-sm-1 col-form-label">:</label>
                            <div className="col-sm-7">
                                <select onChange={e => stateGender(e.target.value)} value={inputGender} className="form-control">
                                    <option value="">Select Gender</option>
                                    <option value="0">Male</option>
                                    <option value="1">Female</option>
                                </select>
                            </div>
                        </div>
                        <div className="form-group row mb-2">
                            <label className="col-sm-4 col-form-label">Alamat</label>
                            <div className="col-sm-12">
                                <textarea name="" className="form-control" value={inputHometown} placeholder={dataMeProfile.hometown}
                                onChange={e => stateHometown(e.target.value)}></textarea>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-4 col-form-label">Tentang Saya</label>
                            <div className="col-sm-12">
                                <textarea name="" className="form-control" value={inputBio} placeholder={dataMeProfile.bio}
                                onChange={e => stateBio(e.target.value)}></textarea>
                            </div>
                        </div>
                        <button type="submit" className="btn btn-success m-1" onClick={(e)=>updateProfile()}>Update Profile</button>
                    </div>
                </div>
            </div>
        </Modal>
    )
}