import Modal from 'react-modal'
import { useEffect, useState } from "react";
import Router from 'next/router';
import axios from 'axios';
import React from 'react';

export default function DataPekerjaan(props:any) {
    const [inputCarrerEnding, stateCarrerEnding] = useState('');
    const [inputCarrerStarting, stateCarrerStarting] = useState('');
    const [inputPosition, statePosition] = useState('');
    const [inputCompanyName, stateCompanyName] = useState('');
    const {
        dataMeProfile, 
        showModal,
        onClose,
        token
    } = props  

    function updatePekerjaan() {        
        axios.post('http://localhost:3000/api/Pekerjaan/pekerjaan', {
            token: token,
            company_name: inputCompanyName,
            starting_from: inputCarrerStarting,
            ending_in: inputCarrerEnding,
            position: inputPosition
        }).then((r) => {
            onClose()
            Router.push('/Home')
        })
        .catch(e => console.log(e))
    }
    

    return (
        <Modal
            isOpen={showModal}
            onRequestClose={onClose}
            ariaHideApp={false}
        >
            <div className="modal-overlay" >
                <div className="modal-style position-relative" style={{ zIndex: "20" }}>
                    <div className="modal-header d-flex">
                        <h4>Edit Data Pekerjaan</h4>
                        <div className="d-flex float-end">
                            <button className="btn btn-danger m-1" onClick={onClose}>Close</button>
                        </div>
                    </div>
                    <div className="modal-body">
                        <div className="form-group row mb-2">
                            <label className="col-sm-4 col-form-label">Nama Perusahaan</label>
                            <label className="col-sm-1 col-form-label">:</label>
                            <div className="col-sm-7">
                                <input type="text" className="form-control" value={inputCompanyName} placeholder={dataMeProfile.career.company_name ?? "Nama Perusahaan"}
                                onChange={e => stateCompanyName(e.target.value)} />
                            </div>
                        </div>
                        <div className="form-group row mb-2">
                            <label className="col-sm-4 col-form-label">Tanggal Mulai Bekerja</label>
                            <label className="col-sm-1 col-form-label">:</label>
                            <div className="col-sm-7">
                                <input type="date" className="form-control" value={inputCarrerStarting} placeholder={dataMeProfile.career.starting_from ?? "Tanggal Mulai Bekerja"}
                                onChange={e => stateCarrerStarting(e.target.value)} />
                            </div>
                        </div>
                        <div className="form-group row mb-2">
                            <label className="col-sm-4 col-form-label">Tanggal Akhir Bekerja</label>
                            <label className="col-sm-1 col-form-label">:</label>
                            <div className="col-sm-7">
                                <input type="date" className="form-control" value={inputCarrerEnding} placeholder={dataMeProfile.career.ending_in ?? "Tanggal Akhir Bekerja"}
                                onChange={e => stateCarrerEnding(e.target.value)} />
                            </div>
                        </div>
                        <div className="form-group row mb-2">
                            <label className="col-sm-4 col-form-label">Posisi Pekerjaan</label>
                            <label className="col-sm-1 col-form-label">:</label>
                            <div className="col-sm-7">
                                <input type="text" className="form-control" value={inputPosition}
                                onChange={e => statePosition(e.target.value)} />
                            </div>
                        </div>
                        <button type="submit" className="btn btn-success m-1" onClick={(e)=>updatePekerjaan()}>Update Data</button>
                    </div>
                </div>
            </div>
        </Modal>
    )
}