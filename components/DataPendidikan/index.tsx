import Modal from 'react-modal'
import { useEffect, useState } from "react";
import Router from 'next/router';
import axios from 'axios';
import React from 'react';

export default function DataPendidikan(props:any) {

    const [inputSchool, stateSchool] = useState('');
    const [inputGraduation, stateGraduation] = useState('');
    const {
        dataMeProfile, 
        showModal,
        onClose,
        token
    } = props  
    
    function updatePendidikan() {
        axios.post('http://localhost:3000/api/Education/education', {
            token: token,
            school_name: inputSchool,
            graduation_time: inputGraduation,
        }).then((r) => {
            onClose()
            Router.push('/Home')
        })
        .catch(e => console.log(e))
    }

    return (
        <Modal
            isOpen={showModal}
            onRequestClose={onClose}
            ariaHideApp={false}
        >
            <div className="modal-overlay" >
                <div className="modal-style position-relative" style={{ zIndex: "20" }}>
                    <div className="modal-header d-flex">
                        <h4>Edit Data Pendidikan</h4>
                        <div className="d-flex float-end">
                            <button className="btn btn-danger m-1" onClick={onClose}>Close</button>
                        </div>
                    </div>
                    <div className="modal-body">
                        <div className="form-group row mb-2">
                            <label className="col-sm-4 col-form-label">Nama Sekolah</label>
                            <label className="col-sm-1 col-form-label">:</label>
                            <div className="col-sm-7">
                                <input type="text" className="form-control" value={inputSchool} placeholder={dataMeProfile.education.school_name}
                                onChange={e => stateSchool(e.target.value)} />
                            </div>
                        </div>
                        <div className="form-group row mb-2">
                            <label className="col-sm-4 col-form-label">Tanggal Lulus</label>
                            <label className="col-sm-1 col-form-label">:</label>
                            <div className="col-sm-7">
                                <input type="date" className="form-control" value={inputGraduation} placeholder={dataMeProfile.education.graduation_time}
                                onChange={e => stateGraduation(e.target.value)} />
                            </div>
                        </div>
                        <button type="submit" className="btn btn-success m-1" onClick={(e)=>updatePendidikan()}>Update Data</button>
                    </div>
                </div>
            </div>
        </Modal>
    )
}